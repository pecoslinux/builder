# A Run from RAM Builder for PecOS Linux

PecOS is a customized version of Alpine Linux suited for building
network appliances.


## Boot Configuration Parameters

### ROOTPASS

This parameter can be used to set the root password for the image.

To generate a root password, you can use the cross-platform python library
passlib. Below is a sample one-liner that will generate a sha512 password
hash as used in the Linux /etc/shadow file.

```
python -c "from passlib.hash import sha512_crypt; import getpass,string,random; pw=getpass.getpass(); print(sha512_crypt.using(salt=''.join([random.choice(string.ascii_letters + string.digits) for _ in range(16)]),rounds=5000).hash(pw) if (pw==getpass.getpass('Confirm: ')) else exit())"
```


