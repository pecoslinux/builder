ARG         BASE

FROM        pecoslinux/aports AS aports

FROM        $BASE
LABEL       maintainer="Eric Miller <eric@tallprairie.com>"

COPY        --from=aports /etc/apk/repositories /etc/apk/repositories
COPY        --from=aports /etc/apk/keys /etc/apk/keys/
COPY        --from=aports /pecos /pecos

RUN         apk update && \
            apk upgrade && \
            apk add --no-cache --no-scripts \
              alpine-base \
              alpine-sdk \
              apk-tools \
              alpine-conf \
              build-base \
              busybox \
              dosfstools \
              fakeroot \
              linux-vanilla \
              sfdisk \
              sgdisk \
              syslinux \
              xorriso \
              zfs-vanilla \
              zfs \
              mkplatform \
              && \
            adduser -D build && \
            addgroup build wheel && \
            addgroup build abuild && \
            echo "%wheel ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers.d/wheel && \
            mkdir -p /var/cache/distfiles && \
            chgrp abuild /var/cache/distfiles && \
            chmod g+w /var/cache/distfiles && \
            chown -R build:build /home/build && \
            true

COPY        entrypoint.sh /
COPY        build.sh /home/build/
RUN         chown -R build:build /home/build

WORKDIR     /home/build
ENTRYPOINT  ["/entrypoint.sh"]
