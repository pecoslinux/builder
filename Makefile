.PHONY: all test build

version=3.8
owner=pecoslinux
name=builder
base=alpine:$(version)

all: build

build: Dockerfile entrypoint.sh
	docker build --build-arg BASE=$(base) --tag $(owner)/$(name):latest \
		--tag $(owner)/$(name):$(version) .

test:
	docker run -it --rm $(owner)/$(name):$(version)

cli:
	docker run -it --rm $(owner)/$(name):$(version)

